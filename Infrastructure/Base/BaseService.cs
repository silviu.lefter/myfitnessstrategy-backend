﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Base
{
    public class BaseService
    {
        protected IMapper Mapper;

        public BaseService()
        {
            Mapper = BaseAutoMapperConfig.Mapper;
        }
    }
}
