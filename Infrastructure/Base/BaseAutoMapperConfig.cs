﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Base
{
    public class BaseAutoMapperConfig
    {
        public static IMapper Mapper { get; set; }
    }
}
