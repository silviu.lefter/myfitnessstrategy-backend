﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Infrastructure.Base
{
    [ApiController]
    public class BaseController : Controller
    {
        public BaseController()
        {

        }

        protected int GetUserId()
        {
                var identity = HttpContext.User.Identity as ClaimsIdentity;

                var patientId = Int32.Parse(identity.FindFirst("AccountId").Value);

                return patientId;
        }

        protected IActionResult ModelInvalidState()
        {
            return BadRequest(new { Status = "Please complete all fields." });
        }
    }
}
