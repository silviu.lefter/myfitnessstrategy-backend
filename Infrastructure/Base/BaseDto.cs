﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Base
{
    public class BaseDto
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public DateTime UpdateAt { get; set; } = DateTime.Now;

        public bool IsDeleted { get; set; }
    }
}
