﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Base
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        T GetRawById(int id);
        Task<T> GetRawByIdAsync(int id);
        IEnumerable<T> GetRawList(int skip, int take);
        Task<IEnumerable<T>> GetRawListAsync(int skip, int take);
        T Insert(T entity);
        Task<T> InsertAsync(T entity);
        List<T> InsertBulk(List<T> entities);
        Task<List<T>> InsertBulkAsync(List<T> entities);
        int Update(T newEntity);
        Task<int> UpdateAsync(T newEntity);
        Task<int> UpdateBulkAsync(List<T> newEntities);
        Task<int> HardRemoveAsync(T entity);
        Task<int> HardRemoveBulkAsync(List<T> entities);
    }
}
