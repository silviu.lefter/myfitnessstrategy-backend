﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Infrastructure.Base
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected readonly DbContext _rawContext;

        public BaseRepository(DbContext context)
        {
            _rawContext = context;
        }
        public virtual T GetRawById(int id)
        {
            return _rawContext.Set<T>()
                .Where(x => x.Id == id)
                .AsNoTracking()
                .First();
        }

        public virtual async Task<T> GetRawByIdAsync(int id)
        {
            return await _rawContext.Set<T>()
                .Where(x => x.Id == id)
                .AsNoTracking()
                .FirstAsync();
        }

        public virtual IEnumerable<T> GetRawList(int skip, int take)
        {
            return _rawContext.Set<T>()
                .Skip(skip)
                .Take(take)
                .AsEnumerable();
        }

        public virtual async Task<IEnumerable<T>> GetRawListAsync(int skip, int take)
        {
            return await _rawContext.Set<T>()
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        public virtual async Task<int> HardRemoveAsync(T entity)
        {
            _rawContext.Set<T>().Remove(entity);
            return await _rawContext.SaveChangesAsync();
        }

        public virtual async Task<int> HardRemoveBulkAsync(List<T> entities)
        {
            _rawContext.Set<T>().RemoveRange(entities);
            return await _rawContext.SaveChangesAsync();
        }

        public T Insert(T entity)
        {
            SetCreationTimestamp(entity);

            _rawContext.Set<T>().Add(entity);
            _rawContext.SaveChanges();
            return entity;
        }

        public virtual async Task<T> InsertAsync(T entity)
        {
            SetCreationTimestamp(entity);

            await _rawContext.Set<T>().AddAsync(entity);
            await _rawContext.SaveChangesAsync();
            return entity;
        }

        public virtual List<T> InsertBulk(List<T> entities)
        {
            if(entities == null || entities.Count == 0)
            {
                return new List<T>();
            }

            entities.ForEach(entity => SetCreationTimestamp(entity));

            _rawContext.Set<T>().AddRange(entities);
            _rawContext.SaveChanges();

            return entities;
        }

        public virtual async Task<List<T>> InsertBulkAsync(List<T> entities)
        {
            if (entities == null || entities.Count == 0)
            {
                return new List<T>();
            }

            entities.ForEach(entity => SetCreationTimestamp(entity));

            await _rawContext.AddRangeAsync(entities);
            await _rawContext.SaveChangesAsync();

            return entities;
        }

        public int Update(T newEntity)
        {
            _rawContext.Set<T>().Update(newEntity);

            return _rawContext.SaveChanges();
        }

        public virtual async Task<int> UpdateAsync(T newEntity)
        {
            _rawContext.Set<T>().Update(newEntity);

            return await _rawContext.SaveChangesAsync();
        }

        public virtual async Task<int> UpdateBulkAsync(List<T> newEntities)
        {
            _rawContext.Set<T>().UpdateRange(newEntities);

            return await _rawContext.SaveChangesAsync();
        }

        private T SetCreationTimestamp(T entity)
        {
            entity.CreatedAt = DateTime.Now;

            return entity;
        }
    }
}
