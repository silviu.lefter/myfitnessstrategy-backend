﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Config
{
    public class CaloriesNinjaSettings
    {
        public string ApiKey { get; set; }

        public string BaseUrl { get; set; }
    }
}
