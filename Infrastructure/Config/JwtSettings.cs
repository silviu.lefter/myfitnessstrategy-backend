﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Config
{
    public class JwtSettings
    {
        public string Issuer { get; set; }
        public string Secret { get; set; }
        public int AccessTokenMinutesLifetime { get; set; }
        public int RefreshTokenDaysLifetime { get; set; }
    }
}
