﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Exceptions
{
    public class AccountExistingException:Exception
    {
        public AccountExistingException(string message): base(message)
        {

        }
    }
}
