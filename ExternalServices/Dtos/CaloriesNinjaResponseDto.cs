﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalServices.Dtos
{
    public class CaloriesNinjaResponseDto
    {
        public List<FoodNutritionDataDto> Items { get; set; }
    }
}
