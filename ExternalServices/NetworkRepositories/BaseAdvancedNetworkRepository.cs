﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;

namespace ExternalServices.NetworkRepositories
{
    public class BaseAdvancedNetworkRepository : BaseNetworkRepository
    {
        public T SendGet<T>(string url, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var response = Send<T>(url, Method.GET, null, queryParams, headers);

            return response;
        }

        public T SendPost<T>(string url, object payload, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var response = Send<T>(url, Method.POST, payload, queryParams, headers);

            return response;
        }
        public T SendPut<T>(string url, object payload, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var response = Send<T>(url, Method.PUT, payload, queryParams, headers);

            return response;
        }
        public T SendDelete<T>(string url, object payload, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var response = Send<T>(url, Method.DELETE, payload, queryParams, headers);

            return response;
        }

        protected virtual T Send<T>(string url, Method method, object payload, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var response = SendRequest(url, method, payload, queryParams, headers);

            if (response.StatusCode >= HttpStatusCode.OK && response.StatusCode <= HttpStatusCode.IMUsed)
            {
                var responeObject = JsonConvert.DeserializeObject<T>(response.Content);
                return responeObject;
            }
            else
            {
                throw new Exception($"Network Exception: failed to send HTTP request to {url}. Reason: {response.StatusCode} {response.ErrorMessage}");
            }
        }
    }
}
