﻿using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Collections.Generic;
using System.Linq;

namespace ExternalServices.NetworkRepositories
{
    public class BaseNetworkRepository
    {
        public IRestResponse SendRequest(string Url, Method method, object payload, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var request = BuildRequest(method, payload, queryParams, headers);
            var client = BuildClient(Url);

            var response = client.Execute(request);

            return response;
        }

        private RestRequest BuildRequest(Method method, object payload, Dictionary<string, string> queryParams = null, Dictionary<string, string> headers = null)
        {
            var request = new RestRequest(method)
            {
                RequestFormat = DataFormat.Json
            };

            request = AddBaseRequestHeaders(request);

            if (payload != null)
                request.AddJsonBody(payload);

            queryParams?.Keys.ToList().ForEach(key =>
            {
                request.AddQueryParameter(key, queryParams[key]);
            });

            headers?.Keys.ToList().ForEach(key =>
            {
                request.AddHeader(key, headers[key]);
            });

            return request;
        }

        private RestClient BuildClient(string Url,bool useNewtonsoftJson = true)
        {
            var client = new RestClient(Url);

            if (useNewtonsoftJson)
                client.UseNewtonsoftJson();

            return client;
        }

        private RestRequest AddBaseRequestHeaders(RestRequest request)
        {
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            return request;
        }
    }
}
