﻿using ExternalServices.Dtos;
using Infrastructure.Config;
using System.Collections.Generic;

namespace ExternalServices.NetworkRepositories
{
    public class CaloriesNinjaRepository : BaseAdvancedNetworkRepository
    {
        private static string NutritionUrl { get; } = AppConfig.CaloriesNinjaSettings.BaseUrl + "/nutrition";

        public CaloriesNinjaResponseDto GetNutritionData(string foodName)
        {
            var headers = BuildHeaders();
            var queryParams = BuildQueryParams(foodName);

            var response = SendGet<CaloriesNinjaResponseDto>(NutritionUrl, queryParams, headers);

            return response;
        }

        private Dictionary<string,string> BuildHeaders()
        {
            return new Dictionary<string, string>
            {
                {"X-Api-Key",AppConfig.CaloriesNinjaSettings.ApiKey}
            };
        }

        private Dictionary<string,string> BuildQueryParams(string foodName)
        {
            return new Dictionary<string, string>
            {
                {"query",foodName}
            };
        }
    }
}
