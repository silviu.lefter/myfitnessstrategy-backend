﻿using Core.Dtos;
using Core.Dtos.UserActivity;
using Core.Services;
using Infrastructure.Base;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/user-activity")]
    [ApiController]
    public class UserActivityController : BaseController
    {
        private UserActivityService _userActivityService { get; set; }
        public UserActivityController(UserActivityService userActivityService)
        {
            _userActivityService = userActivityService;
        }

        [HttpPost("add-food")]
        public IActionResult AddUserFood([FromBody]UserFoodDto userFood)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            _userActivityService.AddUserFood(userFood);

            return Ok();
        }

        [HttpGet("daily-nutrients-data")]
        public IActionResult DailyNutrientsData([FromQuery] int userId)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            var response = _userActivityService.GetDailyNutrientsData(userId);

            return Ok(response);
        }

        [HttpGet("daily-calories-intake")]
        public IActionResult DailyCaloriesIntake([FromQuery] int userId)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            var response = _userActivityService.GetDailyCaloriesIntake(userId);

            return Ok(response);
        }

        [HttpGet("user-weight-history")]

        public IActionResult WeightHistory([FromQuery] int userId)
        {
            var response = _userActivityService.UserWeightHistory(userId);

            return Ok(response);
        }

        [HttpPost("add-weight")]
        public IActionResult AddWeight([FromBody]UserWeightPayload payload)
        {
            if(!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            _userActivityService.AddUserWeight(payload);

            return Ok();
        }

        [HttpPost("user-food-history")]
        public IActionResult GetUserFoodHistory([FromBody] UserFoodHistoryPayload payload)
        {
            var response = _userActivityService.UserFoodHistory(payload.UserId, payload.Date);

            return Ok(response);
        }

        [HttpGet("last-7-days-statistics")]
        public IActionResult GetLast7DaysStatistics([FromQuery] int userId)
        {
            var response = _userActivityService.GetRecentHistory(userId);

            return Ok(response);
        }
    }
}
