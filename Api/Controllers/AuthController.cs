﻿using Infrastructure.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Threading.Tasks;
using Core.Dtos;
using Core.Services;

namespace Api.Controllers
{
    [Route("api/auth")]
    public class AuthController : BaseController
    {
        private readonly AuthService _authService;
        public AuthController(AuthService authService) : base()
        {
            _authService = authService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody]AuthLoginDto data)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            var response = await _authService.LoginAsync(data);

            if(response.IsSuccessful)
            {
                return Ok(response);
            }

            return Unauthorized(response);
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody]AuthRegisterDto data)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            var response = await _authService.RegisterAsync(data);

            if (response.IsSuccessful)
            {
                return Ok(response);
            }

            return BadRequest(response);
        }
    }
}
