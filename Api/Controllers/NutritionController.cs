﻿using Core.Dtos.Nutrition;
using Infrastructure.Base;
using Microsoft.AspNetCore.Mvc;
using Core.Services;
using ExternalServices.NetworkRepositories;

namespace Api.Controllers
{
    [Route("api/nutrition")]
    public class NutritionController : BaseController
    {
        private NutritionService _nutritionService { get; set; }
        private CaloriesNinjaRepository _caloriesNinjaRepository { get; set; }
        public NutritionController(
            NutritionService nutritionService,

            CaloriesNinjaRepository caloriesNinjaRepository)
        {
            _nutritionService = nutritionService;
            _caloriesNinjaRepository = caloriesNinjaRepository;
        }

        [HttpPost("submit-user-data")]
        public IActionResult SubmitUserData([FromBody] UserDataDto data)
        {
            var response = _nutritionService.SubmitUserData(data);

            return Ok(response);
        }

        [HttpPost("submit-user-goal")]
        public IActionResult SubmitUserGoal([FromBody] UserMacrosGoalDto data)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            _nutritionService.SubmitNutritionGoalData(data);

            return Ok();
        }


        [HttpGet("user-nutrition-target")]
        public IActionResult GetUserNutritionTarget([FromQuery] int userId)
        {
            var response = _nutritionService.GetNutritionTarget(userId);

            return Ok(response);
        }

        [HttpGet("food-nutrition-data")]
        public IActionResult GetFoodNutritionData([FromQuery] string foodName)
        {
            var response = _caloriesNinjaRepository.GetNutritionData(foodName);

            return Ok(response);
        }
    }
}
