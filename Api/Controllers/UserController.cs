﻿using Core.Services;
using Infrastructure.Base;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : BaseController
    {
        private UserService _userService { get; set; }

        public UserController(UserService userActivityService)
        {
            _userService = userActivityService;
        }

        [HttpGet("user-name")]
        public async Task<IActionResult> GetUserName([FromQuery] int userId)
        {
            if (!ModelState.IsValid)
            {
                return ModelInvalidState();
            }

            var response = await _userService.GetUserData(userId);

            return Ok(response);
        }
    }
}
