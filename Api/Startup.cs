using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Api.Middlewares;
using Core.Mapping;
using Infrastructure.Config;
using System;

namespace Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            AppConfig.Init(configuration);
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext();
            services.AddControllers();
            services.AddCoreModules();
            services.AddSwagger();
            services.AddIdentityAuth();

            AutoMapperConfig.Initialize();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();

            app.UseSwaggerApp();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
