﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Core.Database.Context;
using Core.Database.Entities;
using Microsoft.AspNetCore.Identity;
using Core.Services;
using Core.Database.Repositories;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Config;
using ExternalServices.NetworkRepositories;

namespace Api
{
    public static class StartupConfig
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "myFitnessStrategy API", Version = "v1" });
            });
        }

        public static void UseSwaggerApp(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "myFitnessStrategy API");
            });
        }

        public static void AddDbContext(this IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(options => 
            options.UseMySql(AppConfig.ConnectionStrings.MainDatabase));
        }

        public static void AddIdentityAuth(this IServiceCollection services)
        {
            services.AddIdentity<Account, AuthRole>()
               .AddEntityFrameworkStores<DatabaseContext>()
               .AddDefaultTokenProviders();
        }

        public static void AddCoreModules(this IServiceCollection services)
        {
            services.AddTransient<AccountTokenRepository>();
            services.AddTransient<UserDataRepository>();
            services.AddTransient<UserNutritionTargetRepository>();
            services.AddTransient<CaloriesNinjaRepository>();
            services.AddTransient<UserFoodRepository>();

            services.AddTransient<UserService>();
            services.AddTransient<AuthService>();
            services.AddTransient<NutritionService>();
            services.AddTransient<UserActivityService>();
            services.AddTransient<UserWeightRepository>();
        }

    }
}
