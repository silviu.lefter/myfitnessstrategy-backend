﻿using Infrastructure.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Api.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch(AccountExistingException exception)
            {
                await RespondToException(context,HttpStatusCode.BadRequest, exception.Message);
            }
            catch(ResourceMissingException exception)
            {
                await RespondToException(context, HttpStatusCode.BadRequest, exception.Message);
            }
            catch(Exception exception)
            {
                await RespondToException(context, HttpStatusCode.InternalServerError, exception, "Internal Server Error");
            }
        }

        private Task RespondToException(HttpContext context, HttpStatusCode statusCode, string message)
        {
            context.Response.StatusCode = (int)statusCode;
            context.Response.ContentType = "application/json";

            return context.Response.WriteAsync(JsonConvert.SerializeObject(new 
            {
                Message = message,
                Timestamp = DateTime.UtcNow
            }));
        }

        private Task RespondToException(HttpContext context, HttpStatusCode statusCode,Exception exception,string message)
        {
            context.Response.StatusCode = (int)statusCode;
            context.Response.ContentType = "application/json";

            return context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                Message = message,
                Details = exception.Message,
                Type = exception.GetType().Name,
                Timestamp = DateTime.UtcNow
            }));
        }
    }
}
