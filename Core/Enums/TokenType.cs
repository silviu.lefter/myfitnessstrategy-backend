﻿using System;
using System.Web;

namespace Core.Enums
{
    public enum TokenType
    {
        AccessToken,
        RefreshToken
    }
}
