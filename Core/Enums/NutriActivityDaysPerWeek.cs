﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Enums
{
    public enum NutriActivityDaysPerWeek
    {
        Zero = 0,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven
    }
}
