﻿using Core.Database.Entities;
using Core.Dtos.UserActivity;

namespace Core.Mapping
{
    public static class UserFoodMapping
    {
        public static UserFood ToEntity(this UserFoodDto dto)
        {
            return AutoMapperConfig.Mapper.Map<UserFood>(dto);
        }

        public static UserWeight ToEntity(this UserWeightPayload dto)
        {
            return AutoMapperConfig.Mapper.Map<UserWeight>(dto);
        }

        public static UserWeightPayload ToDto(this UserWeight entity)
        {
            return AutoMapperConfig.Mapper.Map<UserWeightPayload>(entity);
        }
    }
}
