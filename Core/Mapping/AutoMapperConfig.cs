﻿using AutoMapper;
using Core.Database.Entities;
using Core.Dtos;
using Core.Dtos.Nutrition;
using Core.Dtos.UserActivity;
using Infrastructure.Base;



namespace Core.Mapping
{
    public class AutoMapperConfig : BaseAutoMapperConfig
    {
        public static void Initialize()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Account, AccountDto>();

                cfg.CreateMap<UserData, UserNutritionDataResponseDto>();

                cfg.CreateMap<UserMacrosData, MacrosDataDto>();

                cfg.CreateMap<UserMacrosData, UserNutritionTargetDto>();

                cfg.CreateMap<UserFood, UserFoodDto>();
                cfg.CreateMap<UserFoodDto, UserFood>();

                cfg.CreateMap<UserWeightPayload, UserWeight>();
                cfg.CreateMap<UserWeight, UserWeightPayload>();

                InitCustomMapping(cfg);
            });

            Mapper = mapperConfiguration.CreateMapper();
        }
            
        public static void InitCustomMapping(IMapperConfigurationExpression configuration)
        {
              configuration.CreateMap<AuthRegisterDto, Account>()
                .ForMember(e => e.UserName, map => map.MapFrom(e => e.Email));

              configuration.CreateMap<UserDataDto, UserData>()
                .ForMember(e => e.AccountId, map => map.MapFrom(e => e.UserId));

              configuration.CreateMap<UserMacrosGoalDto, UserMacrosData>()
                .ForMember(e => e.AccountId, map => map.MapFrom(e => e.UserId));
        }
    }
}
