﻿using Core.Database.Entities;
using Core.Dtos.Nutrition;


namespace Core.Mapping
{
    public static class UserMacrosGoalMapping
    {
        public static UserMacrosData ToEntity(this UserMacrosGoalDto dto)
        {
            return AutoMapperConfig.Mapper.Map<UserMacrosData>(dto);
        }
    }
}
