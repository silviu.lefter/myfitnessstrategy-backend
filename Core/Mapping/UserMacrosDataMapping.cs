﻿
using Core.Database.Entities;
using Core.Dtos.Nutrition;

namespace Core.Mapping
{
    public static class UserMacrosDataMapping
    {
        public static UserNutritionTargetDto ToDto(this UserMacrosData entity)
        {
            if (entity == null)
                return null;

            return AutoMapperConfig.Mapper.Map<UserNutritionTargetDto>(entity);
        }
    }
}
