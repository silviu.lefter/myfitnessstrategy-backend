﻿using Core.Database.Entities;
using Core.Dtos;

namespace Core.Mapping
{
    public static class AccountMapping
    {
        public static Account ToEntity(this AuthRegisterDto dto)
        {
            return AutoMapperConfig.Mapper.Map<AuthRegisterDto, Account>(dto);
        }

        public static AccountDto ToDto(this Account entity)
        {
            return AutoMapperConfig.Mapper.Map<Account, AccountDto>(entity);
        }
    }
}
