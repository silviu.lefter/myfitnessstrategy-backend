﻿using Core.Database.Entities;
using Core.Dtos.Nutrition;


namespace Core.Mapping
{
    public static class UserDataMapping
    {
        public static UserData ToEntity(this UserDataDto dto)
        {
            return AutoMapperConfig.Mapper.Map<UserData>(dto);
        }

        public static UserNutritionDataResponseDto ToDto(this UserData entity)
        {
            return AutoMapperConfig.Mapper.Map<UserNutritionDataResponseDto>(entity);
        }
    }
}
