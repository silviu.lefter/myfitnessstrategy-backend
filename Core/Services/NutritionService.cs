﻿using Core.Database.Entities;
using Core.Database.Repositories;
using Core.Dtos.Nutrition;
using Core.Enums;
using Core.Logic;
using Core.Mapping;
using Core.Repositories;

namespace Core.Services
{
    public class NutritionService
    {
        private UserDataRepository _userDataRepository { get; set; }
        private UserNutritionTargetRepository _userNutritionTargetRepository { get; set; }

        public NutritionService(
            UserDataRepository nutritionRepository,
            UserNutritionTargetRepository userMacrosDataRepository)
        {
            _userDataRepository = nutritionRepository;
            _userNutritionTargetRepository = userMacrosDataRepository;
        }

        public UserMacrosDataDto SubmitUserData(UserDataDto data)
        {
            var entity = data.ToEntity();

            var nutritionData = _userDataRepository.Insert(entity);

            var macrosData = CalculateMacros(nutritionData);

            return macrosData;
        }

        public void SubmitNutritionGoalData(UserMacrosGoalDto data)
        {
            var entity = data.ToEntity();

            _userNutritionTargetRepository.Insert(entity);
        }

        public UserNutritionTargetDto GetNutritionTarget(int userId)
        {
            var entity = _userNutritionTargetRepository.GetUserNutrientsTarget(userId);

            return entity.ToDto();
        }

        private UserMacrosDataDto CalculateMacros(UserData userData)
        {
            if(userData.MeasuringSystem == NutriMeasuringSystems.Metric)
            {
                MacrosCalculator macrosCalculator = new MacrosCalculator();
                return macrosCalculator.CalculateMacrosIntake(userData);
            }
            else
            {
                ImperialAdapterMacrosCalculator macrosCalculator = new ImperialAdapterMacrosCalculator();
                return macrosCalculator.CalculateMacrosIntake(userData);
            }

        }
    }
}
