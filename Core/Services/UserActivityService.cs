﻿using Core.Database.Entities;
using Core.Database.Repositories;
using Core.Dtos.Nutrition;
using Core.Mapping;
using System;
using System.Collections.Generic;
using Core.Dtos.UserActivity;
using Core.Dtos;
using Core.Repositories;
using System.Globalization;

namespace Core.Services
{
    public class UserActivityService
    {
        private UserFoodRepository _userFoodRepository { get; set; }
        private UserNutritionTargetRepository _userNutritionTargetRepository { get; set; }
        private UserDataRepository _userDataRepository { get; set; }

        private UserWeightRepository _userWeightRepository { get; set; }
        public UserActivityService(
            UserFoodRepository userFoodRepository,
            UserNutritionTargetRepository userNutritionTargetRepository,
            UserWeightRepository userWeightRepository,
            UserDataRepository userDataRepository)
        {
            _userDataRepository = userDataRepository;
            _userFoodRepository = userFoodRepository;
            _userNutritionTargetRepository = userNutritionTargetRepository;
            _userWeightRepository = userWeightRepository;
        }

        public UserWeightHistoryDto UserWeightHistory(int userId)
        {
            return new UserWeightHistoryDto
            {
                UserWeightPayloads = _userWeightRepository.GetUserWeightHistory(userId)
            };
        }

        public UserFoodHistory UserFoodHistory(int userId, string time)
        {
            string[] formats = { "d/M/yyyy HHmmss", "MM/dd/yyyy", "d/MM/yyyy HHmmss", "dd/M/yyyy HHmmss" };
            DateTime dateTime = DateTime.ParseExact(time, formats, CultureInfo.InvariantCulture);

            var result = _userFoodRepository.GetUserFoodHistory(dateTime, userId);


            foreach(var item in result)
            {
                item.Title = item.Title.ToUpper();
            }
            return new UserFoodHistory
            {
                Items = result
            };
        }

        public void AddUserFood(UserFoodDto userFood)
        {
            var entity = userFood.ToEntity();

            _userFoodRepository.Insert(entity);
        }

        public void AddUserWeight(UserWeightPayload data)
        {
            var entity = data.ToEntity();

            _userWeightRepository.Insert(entity);
        }

        public Last7DaysStatisticsDto GetRecentHistory(int userId)
        {
            var result = new Last7DaysStatisticsDto();

            result.NeededCalories = _userDataRepository.GetCaloriesNeeded(userId);

            var date = DateTime.Now.AddDays(-6);

            for(int index=0;index<7;index++)
            {
                result.CaloriesConsumed.Add(new CaloriesConsumedDto
                {
                    Date = date,
                    Calories = _userFoodRepository.DailyCaloriesIntake(userId,date)
                });

                date = date.AddDays(1);
            }

            return result;
        }

        public DailyNutrientsDataDto GetDailyNutrientsData(int userId)
        {
            var userFoods = _userFoodRepository.GetUserFoodsByDate(userId, DateTime.Now.Date);

            var userNutrientsTarget = _userNutritionTargetRepository.GetUserNutrientsTarget(userId);

            return DailyNutrientsData(userFoods, userNutrientsTarget);
        }

        public DailyCaloriesIntakeDto GetDailyCaloriesIntake(int userId)
        {
            var caloriesIntake = _userFoodRepository.DailyCaloriesIntake(userId, DateTime.Now.Date);

            return new DailyCaloriesIntakeDto { CaloriesIntake = caloriesIntake };
        }

        private DailyNutrientsDataDto DailyNutrientsData(List<UserFood> userFoods, UserMacrosData dailyNeededNutrients)
        {
            var result = new DailyNutrientsDataDto();

            result = TodayConsumedNutrients(result, userFoods);
            result = DailyNeededNutrients(result, dailyNeededNutrients);
            result = DailyRemainingNutrients(result);

            return result;
        }

        private DailyNutrientsDataDto TodayConsumedNutrients(DailyNutrientsDataDto dailyNutrientsData, List<UserFood> userFoods)
        {
            foreach (var item in userFoods)
            {
                dailyNutrientsData.TodayCaloriesIntake += item.Calories;
                dailyNutrientsData.TodayCarbsIntake += item.Carbs;
                dailyNutrientsData.TodayFatsIntake += item.Fats;
                dailyNutrientsData.TodayProteinsIntake += item.Proteins;
            }

            return dailyNutrientsData;
        }

        private DailyNutrientsDataDto DailyNeededNutrients(DailyNutrientsDataDto dailyNutrientsData, UserMacrosData userNutrientsTarget)
        {
            dailyNutrientsData.CaloriesNeeded = userNutrientsTarget.DailyCaloriesIntake;
            dailyNutrientsData.CarbsNeeded = userNutrientsTarget.DailyCarbsGrams;
            dailyNutrientsData.ProteinsNeeded = userNutrientsTarget.DailyProteinGrams;
            dailyNutrientsData.FatsNeeded = userNutrientsTarget.DailyFatsGrams;

            return dailyNutrientsData;
        }

        private DailyNutrientsDataDto DailyRemainingNutrients(DailyNutrientsDataDto dailyNutrientsData)
        {
            dailyNutrientsData.CaloriesRemaining = dailyNutrientsData.CaloriesNeeded - dailyNutrientsData.TodayCaloriesIntake;
            dailyNutrientsData.CarbsRemaining = dailyNutrientsData.CarbsNeeded - dailyNutrientsData.TodayCarbsIntake;
            dailyNutrientsData.FatsRemaining = dailyNutrientsData.FatsNeeded - dailyNutrientsData.TodayFatsIntake;
            dailyNutrientsData.ProteinsNeeded = dailyNutrientsData.ProteinsNeeded - dailyNutrientsData.TodayProteinsIntake;

            return dailyNutrientsData;
        }
    }
}
