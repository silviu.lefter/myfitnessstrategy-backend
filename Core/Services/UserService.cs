﻿using Core.Database.Entities;
using Core.Dtos.UserData;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace Core.Services
{
    public class UserService
    {
        private readonly UserManager<Account> _userManager;

        public UserService(UserManager<Account> userManager)
        {
            _userManager = userManager;
        }

        public async Task<UserDataDto> GetUserData(int userId)
        {
            var user = await _userManager.FindByIdAsync(Convert.ToString(userId));

            return new UserDataDto
            {
                Name = user.FirstName + " " + user.LastName
            };
        }
    }
}
