﻿using Core.Database.Entities;
using Core.Dtos;
using Core.Mapping;
using Infrastructure.Base;
using Infrastructure.Exceptions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Core.Services
{
    public class AuthService : BaseService
    {
        private readonly SignInManager<Account> _signInManager;
        private readonly UserManager<Account> _userManager;

        public AuthService(
            SignInManager<Account> signInManager,
            UserManager<Account> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<AuthResponseDto> LoginAsync(AuthLoginDto payload)
        {
            var loginResult = await _signInManager.PasswordSignInAsync(payload.Email, payload.Password, true, true);

            var response = CreateLoginResponse(loginResult);

            if(loginResult.Succeeded)
            {
                var account = await _userManager.FindByEmailAsync(payload.Email);
                response.AccountId = account.Id;
            }

            return response;
        }

        public async Task<AuthResponseDto> RegisterAsync(AuthRegisterDto registerInfo)
        {
            if(!CheckEmail(registerInfo.Email))
            {
                return new AuthResponseDto
                {
                    IsSuccessful = false,
                    Message = "Email format is wrong"
                };
            }

            var accountExisting = await  _userManager.FindByEmailAsync(registerInfo.Email);

            if(accountExisting != null)
            {
                return new AuthResponseDto
                {
                    IsSuccessful = false,
                    Message = "An account with this email already exists."
                };
            }

            var account = registerInfo.ToEntity();
            var accountCreatingResult = await _userManager.CreateAsync(account,registerInfo.Password);

            var response = CreateRegisterResponse(accountCreatingResult);

            if (accountCreatingResult.Succeeded)
            {
                response.AccountId = account.Id;
            }

            return response;
        }

        private AuthResponseDto CreateLoginResponse(SignInResult result)
        {
            return new AuthResponseDto
            {
                IsSuccessful = result.Succeeded,
                Message = result.Succeeded ? "You Have Successfully Logged In !" : "The username or password is incorrect !"
            };
        }

        private AuthResponseDto CreateRegisterResponse(IdentityResult result)
        {
            return new AuthResponseDto
            {
                IsSuccessful = result.Succeeded,
                Message = result.Succeeded ? "You have successfully register !" : result.Errors.FirstOrDefault().Description
            };
        }

        private bool CheckEmail(string email)
        {                         
            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
