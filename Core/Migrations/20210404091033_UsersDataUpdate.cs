﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Migrations
{
    public partial class UsersDataUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsersNutritionData_Accounts_AccountId",
                table: "UsersNutritionData");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UsersNutritionData",
                table: "UsersNutritionData");

            migrationBuilder.RenameTable(
                name: "UsersNutritionData",
                newName: "UsersData");

            migrationBuilder.RenameIndex(
                name: "IX_UsersNutritionData_AccountId",
                table: "UsersData",
                newName: "IX_UsersData_AccountId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UsersData",
                table: "UsersData",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UsersData_Accounts_AccountId",
                table: "UsersData",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsersData_Accounts_AccountId",
                table: "UsersData");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UsersData",
                table: "UsersData");

            migrationBuilder.RenameTable(
                name: "UsersData",
                newName: "UsersNutritionData");

            migrationBuilder.RenameIndex(
                name: "IX_UsersData_AccountId",
                table: "UsersNutritionData",
                newName: "IX_UsersNutritionData_AccountId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UsersNutritionData",
                table: "UsersNutritionData",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UsersNutritionData_Accounts_AccountId",
                table: "UsersNutritionData",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
