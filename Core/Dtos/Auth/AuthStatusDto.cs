﻿
namespace Core.Dtos
{
    public class AuthResponseDto
    {
        public string Message { get; set; }
        public bool IsSuccessful { get; set; }
        public int AccountId { get; set; }
    }
}
