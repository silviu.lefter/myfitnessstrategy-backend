﻿using Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class AccountDto : BaseDto
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
