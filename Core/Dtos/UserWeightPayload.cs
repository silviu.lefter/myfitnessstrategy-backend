﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos.UserActivity
{
    public class UserWeightPayload
    {
        public int AccountId { get; set; }
        public float Value { get; set; }
    }
}
