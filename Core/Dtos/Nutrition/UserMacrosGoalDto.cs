﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos.Nutrition
{
    public class UserMacrosGoalDto
    {
        public int UserId { get; set; }
        public int DailyCaloriesIntake { get; set; }
        public int DailyCarbsIntake { get; set; }
        public int DailyCaloriesFats { get; set; }
        public int DailyProteinIntake { get; set; }

        public int DailyCarbsGrams { get; set; }
        public int DailyFatsGrams { get; set; }
        public int DailyProteinGrams { get; set; }
    }
}
