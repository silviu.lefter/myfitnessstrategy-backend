﻿
namespace Core.Dtos.Nutrition
{
    public class MacrosDataDto
    {
        public int DailyCaloriesIntake { get; set; }
        public int DailyCarbsCaloriesIntake { get; set; }
        public int DailyFatsCaloriesIntake { get; set; }
        public int DailyProteinCaloriesIntake { get; set; }

        public int DailyCarbsGramsIntake { get; set; }
        public int DailyFatsGramsIntake { get; set; }
        public int DailyProteinGramsIntake { get; set; }
    }
}
