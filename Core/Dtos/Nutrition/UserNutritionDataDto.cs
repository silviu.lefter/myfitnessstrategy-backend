﻿
using Core.Enums;

namespace Core.Dtos.Nutrition
{
    public class UserDataDto
    {
        public int UserId { get; set; }
        public Sex Sex { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public NutriMeasuringSystems MeasuringSystem { get; set; }
        public NutriDailyActivityLevels DailyActivityLevel { get; set; }
        public NutriActivityDaysPerWeek ActivityDaysPerWeek { get; set; }
        public NutriExerciseLevels ExerciseLevel { get; set; }
    }
}
