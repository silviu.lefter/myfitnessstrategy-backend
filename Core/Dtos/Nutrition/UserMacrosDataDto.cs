﻿
namespace Core.Dtos.Nutrition
{
    public class UserMacrosDataDto
    {
        public int AccountId { get; set; }
        public MacrosDataDto FatLossMacrosData { get; set; }
        public MacrosDataDto MaintenanceMacrosData { get; set; }
        public MacrosDataDto MassGainMacrosData { get; set; }
    }
}
