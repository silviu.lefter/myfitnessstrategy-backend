﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos.Nutrition
{
    public class DailyCaloriesIntakeDto
    {
        public int CaloriesIntake { get; set; }
    }
}
