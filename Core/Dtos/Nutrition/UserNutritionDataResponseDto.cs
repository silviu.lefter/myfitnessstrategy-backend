﻿using System;

namespace Core.Dtos.Nutrition
{
    public class UserNutritionDataResponseDto
    {
        public int UserId { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public int DailyCalorieIntake { get; set; }
    }
}
