﻿using System.Collections.Generic;

namespace Core.Dtos
{
    public class UserFoodHistory
    {
        public List<UserFoodItemDto> Items { get; set; }
    }
}
