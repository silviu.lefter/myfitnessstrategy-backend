﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos.UserActivity
{
    public class DailyNutrientsDataDto
    {
        public int CaloriesNeeded { get; set; }
        public int ProteinsNeeded { get; set; }
        public int CarbsNeeded { get; set; }
        public int FatsNeeded { get; set; }
        public int TodayCaloriesIntake { get; set; }
        public int TodayProteinsIntake { get; set; }
        public int TodayCarbsIntake { get; set; }
        public int TodayFatsIntake { get; set; }
        public int CaloriesRemaining { get; set; }
        public int ProteinsRemaining { get; set; }
        public int FatsRemaining { get; set; }
        public int CarbsRemaining { get; set; }
    }
}
