﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class UserFoodItemDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int Calories { get; set; }
        public int Carbs { get; set; }
        public int Fats { get; set; }

        public int Proteins { get; set; }
    }
}
