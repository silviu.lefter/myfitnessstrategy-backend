﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class Last7DaysStatisticsDto
    {
        public List<CaloriesConsumedDto> CaloriesConsumed { get; set; } = new List<CaloriesConsumedDto>();
        public int NeededCalories { get; set; }
    }
}
