﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class UserFoodHistoryPayload
    {
        public int UserId { get; set; }

        public string Date { get; set; }
    }
}
