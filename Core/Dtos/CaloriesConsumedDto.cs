﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class CaloriesConsumedDto
    {
        public DateTime Date { get; set; }

        public int Calories { get; set; }
    }
}
