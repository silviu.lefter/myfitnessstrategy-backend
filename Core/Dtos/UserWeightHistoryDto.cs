﻿using Core.Dtos.UserActivity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class UserWeightHistoryDto
    {
        public List<UserWeightDto> UserWeightPayloads { get; set; }
    }
}
