﻿using System;

namespace Core.Dtos.UserActivity
{
    public class UserFoodDto
    {
        public int AccountId { get; set; }
        public string FoodName { get; set; }
        public int Calories { get; set; }
        public int Fats { get; set; }
        public int Proteins { get; set; }
        public int Carbs { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
