﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos.UserActivity
{
    public class UserWeightDto
    {
        public float Value { get; set; }

        public string CreatedAt { get; set; }
    }
}
