﻿using Core.Database.Entities;
using Core.Dtos.Nutrition;
using Core.Enums;

namespace Core.Logic
{
    public class MacrosCalculator
    {
        public UserMacrosDataDto CalculateMacrosIntake(UserData data)
        {
            var result = new UserMacrosDataDto();

            result.AccountId = data.AccountId;

            result.MaintenanceMacrosData = CalculateMaintenanceMacros(data);
            result.FatLossMacrosData = CalculateFatLossMacros(result.MaintenanceMacrosData, data);
            result.MassGainMacrosData = CalculateMassGainMacros(result.MaintenanceMacrosData, data);

            return result;
        }
        private MacrosDataDto CalculateMaintenanceMacros(UserData data)
        {
            var result = new MacrosDataDto();

            if (data.ExerciseLevel >= NutriExerciseLevels.Intense && data.ActivityDaysPerWeek >= NutriActivityDaysPerWeek.Four && data.DailyActivityLevel >= NutriDailyActivityLevels.VeryActive)
            {
                result.DailyCaloriesIntake = (int)(16 * 2.2 * data.Weight);
            }
            else if (data.ExerciseLevel >= NutriExerciseLevels.Moderate && data.ActivityDaysPerWeek >= NutriActivityDaysPerWeek.Three && data.DailyActivityLevel >= NutriDailyActivityLevels.Active)
            {
                result.DailyCaloriesIntake = (int)(15 * 2.2 * data.Weight);
            }
            else if (data.ExerciseLevel >= NutriExerciseLevels.Light && data.ActivityDaysPerWeek >= NutriActivityDaysPerWeek.Three && data.DailyActivityLevel >= NutriDailyActivityLevels.Active)
            {
                result.DailyCaloriesIntake = (int)(14 * 2.2 * data.Weight);
            }
            else
            {
                result.DailyCaloriesIntake = (int)(13 * 2.2 * data.Weight);
            }

            result.DailyProteinCaloriesIntake = (int)(0.8 * 2.2 * data.Weight) * 4;
            result.DailyFatsCaloriesIntake = (int)(0.25 * result.DailyCaloriesIntake);
            result.DailyCarbsCaloriesIntake = result.DailyCaloriesIntake - result.DailyProteinCaloriesIntake - result.DailyFatsCaloriesIntake;

            result.DailyCarbsGramsIntake = result.DailyCarbsCaloriesIntake / 4;
            result.DailyFatsGramsIntake = result.DailyFatsCaloriesIntake / 9;
            result.DailyProteinGramsIntake = result.DailyProteinCaloriesIntake / 4;


            return result;
        }

        private MacrosDataDto CalculateFatLossMacros(MacrosDataDto macrosData, UserData data)
        {
            var result = new MacrosDataDto();

            result.DailyCaloriesIntake = (int)(macrosData.DailyCaloriesIntake - (0.25 * macrosData.DailyCaloriesIntake));
            result.DailyProteinCaloriesIntake = (int)(1 * 2.2 * data.Weight) * 4;
            result.DailyFatsCaloriesIntake = (int)(0.25 * result.DailyCaloriesIntake);
            result.DailyCarbsCaloriesIntake = result.DailyCaloriesIntake - result.DailyProteinCaloriesIntake - result.DailyFatsCaloriesIntake;

            result.DailyCarbsGramsIntake = result.DailyCarbsCaloriesIntake / 4;
            result.DailyFatsGramsIntake = result.DailyFatsCaloriesIntake / 9;
            result.DailyProteinGramsIntake = result.DailyProteinCaloriesIntake / 4;

            return result;
        }

        private MacrosDataDto CalculateMassGainMacros(MacrosDataDto macrosData, UserData data)
        {
            var result = new MacrosDataDto();

            result.DailyCaloriesIntake = (int)(1.1 * macrosData.DailyCaloriesIntake);
            result.DailyProteinCaloriesIntake = (int)(0.8 * 2.2 * data.Weight) * 4;
            result.DailyFatsCaloriesIntake = (int)(0.25 * result.DailyCaloriesIntake);
            result.DailyCarbsCaloriesIntake = result.DailyCaloriesIntake - result.DailyProteinCaloriesIntake - result.DailyFatsCaloriesIntake;

            result.DailyCarbsGramsIntake = result.DailyCarbsCaloriesIntake / 4;
            result.DailyFatsGramsIntake = result.DailyFatsCaloriesIntake / 9;
            result.DailyProteinGramsIntake = result.DailyProteinCaloriesIntake / 4;

            return result;
        }
    }
}
