﻿using Core.Database.Entities;
using Core.Dtos.Nutrition;

namespace Core.Logic
{
    class ImperialAdapterMacrosCalculator 
    {
        private MacrosCalculator _macrosCalculator { get; set; }
        public ImperialAdapterMacrosCalculator()
        {
            _macrosCalculator = new MacrosCalculator();
        }

        public UserMacrosDataDto CalculateMacrosIntake(UserData data)
        {
            data.Weight = (int)(data.Weight / 2.2);

            return _macrosCalculator.CalculateMacrosIntake(data);
        }
    }
}
