﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Core.Database.Entities
{
    public class AccountToken : IdentityUserToken<int>
    {
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime ExpiryDate { get; set; }
        public bool Invalidated { get; set; }
    }
}
