﻿using Core.Enums;
using Infrastructure.Base;
using System;

namespace Core.Database.Entities
{
    public class UserData : BaseEntity
    {
        public Sex Sex { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
        public NutriMeasuringSystems MeasuringSystem { get; set; }
        public NutriDailyActivityLevels DailyActivityLevel { get; set; }
        public NutriActivityDaysPerWeek ActivityDaysPerWeek { get; set; }
        public NutriExerciseLevels ExerciseLevel { get; set; }
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
