﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Core.Database.Entities
{
    public class Account : IdentityUser<int>
    {
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
        public bool IsDeleted { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
