﻿using Infrastructure.Base;
using System;

namespace Core.Database.Entities
{
    public class UserMacrosData : BaseEntity
    {
        public DateTime UpdateAt { get; set; } = DateTime.UtcNow;
        public int DailyCaloriesIntake { get; set; }
        public int DailyProteinIntake { get; set; }
        public int DailyCarbsIntake { get; set; }
        public int DailyCaloriesFats { get; set; }
        public int DailyProteinGrams { get; set; }
        public int DailyCarbsGrams { get; set; }
        public int DailyFatsGrams { get; set; }
        public Account Account { get; set; }
        public int AccountId { get; set; }
    }
}
