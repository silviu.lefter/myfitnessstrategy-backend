﻿using Infrastructure.Base;

namespace Core.Database.Entities
{
    public class UserFood : BaseEntity
    {
        public string FoodName { get; set; }
        public int Calories { get; set; }
        public int Proteins { get; set; }
        public int Fats { get; set; }
        public int Carbs { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }
    }
}
