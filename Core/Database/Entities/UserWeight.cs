﻿using Infrastructure.Base;
using System;

namespace Core.Database.Entities
{
    public class UserWeight : BaseEntity
    {
        public int Value { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }
    }
}
