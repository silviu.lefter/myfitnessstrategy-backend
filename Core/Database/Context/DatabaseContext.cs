﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Core.Database.Entities;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Config;

namespace Core.Database.Context
{
    public class DatabaseContext : IdentityDbContext<Account, AuthRole, int, AccountClaim, AccountRole, AccountLogin, AuthRoleClaim, AccountToken>
    {
        public DatabaseContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(AppConfig.ConnectionStrings.MainDatabase);
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Identity
            modelBuilder.Entity<Account>().ToTable("Accounts");
            modelBuilder.Entity<AuthRole>().ToTable("AuthRoles");
            modelBuilder.Entity<AccountRole>().ToTable("AccountRoles");
            modelBuilder.Entity<AccountLogin>().ToTable("AccountLogins");
            modelBuilder.Entity<AccountToken>().ToTable("AccountTokens");
            modelBuilder.Entity<AccountClaim>().ToTable("AccountClaims");
            modelBuilder.Entity<AuthRoleClaim>().ToTable("AuthRoleClaims");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.FirstName).HasMaxLength(200);
                entity.Property(e => e.LastName).HasMaxLength(200);
                entity.Property(e => e.Email).HasMaxLength(200);
            });

            modelBuilder.Entity<AuthRole>(entity =>
            {
                entity.Ignore(e => e.ConcurrencyStamp);
            });
            #endregion
        }

        public DbSet<UserData> UsersData { get; set; }
        public DbSet<UserMacrosData> UserMacrosData { get; set; }
        public DbSet<UserFood> UserFoods { get; set; }
        public DbSet<UserWeight> UserWeights { get; set; }
    }
}
