﻿using Microsoft.EntityFrameworkCore.Design;
using Infrastructure.Config;
using System;

namespace Core.Database.Context
{
    public class DatabaseContextMigrationsFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            AppConfig.MigrationsInit();

            return new DatabaseContext();
        }
    }
}
