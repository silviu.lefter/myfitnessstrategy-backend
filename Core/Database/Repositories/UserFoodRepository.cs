﻿using Core.Database.Context;
using Core.Database.Entities;
using Core.Dtos;
using Infrastructure.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Database.Repositories
{
    public class UserFoodRepository : BaseRepository<UserFood>
    {
        private DatabaseContext _db { get; set; }
        public UserFoodRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
            this._db = databaseContext;
        }

        public List<UserFood> GetUserFoodsByDate(int userId,DateTime dateTime)
        {
            return _db.UserFoods
                .Where(e => e.AccountId == userId)
                .Where(e => e.CreatedAt.Date == dateTime.Date)
                .ToList();
        }

        public int DailyCaloriesIntake(int userId, DateTime dateTime)
        {
            return _db.UserFoods
                .Where(e => e.AccountId == userId)
                .Where(e => e.CreatedAt.Date == dateTime.Date)
                .Select(e => e.Calories)
                .Sum();
        }

        public List<UserFoodItemDto> GetUserFoodHistory(DateTime dateTime, int userId)
        {
            return _db.UserFoods
                .Where(e => e.AccountId == userId)
                .Where(e => e.CreatedAt.Date == dateTime.Date)
                .Select(e => new UserFoodItemDto
                {
                    Title = e.FoodName,
                    Description = e.FoodName,
                    Calories = e.Calories,
                    Proteins = e.Proteins,
                    Fats = e.Fats,
                    Carbs = e.Carbs
                })
                .ToList();
        }
    }
}
