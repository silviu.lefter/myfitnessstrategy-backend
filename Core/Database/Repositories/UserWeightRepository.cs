﻿using Core.Database.Context;
using Core.Database.Entities;
using Core.Dtos.UserActivity;
using Infrastructure.Base;
using System.Collections.Generic;
using System.Linq;

namespace Core.Database.Repositories
{
    public class UserWeightRepository : BaseRepository<UserWeight>
    {
        private DatabaseContext _db { get; set; }
        public UserWeightRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
            this._db = databaseContext;
        }

        public List<UserWeightDto> GetUserWeightHistory(int userId)
        {
            return _db.UserWeights
                .Where(e => e.AccountId == userId)
                .OrderBy(e => e.CreatedAt)
                .Select(e => new UserWeightDto
                {
                    CreatedAt = e.CreatedAt.ToString("dd-MM-yyyy"),
                    Value = e.Value
                })
                .ToList();
        }
    }
}
