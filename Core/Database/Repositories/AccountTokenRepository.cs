﻿using Core.Database.Context;
using Core.Database.Entities;
using Infrastructure.Base;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Database.Repositories
{
    public class AccountTokenRepository
    {
        private readonly DatabaseContext _db;

        public AccountTokenRepository(DatabaseContext db)
        {
            _db = db;
        }

        public AccountToken Insert(AccountToken entity)
        {
            _db.UserTokens.Add(entity);
            _db.SaveChanges();

            return entity;
        }

        public void InvalidatePastTokens(int accountId)
        {
            var tokens = _db.Set<AccountToken>()
                .Where(art => art.UserId == accountId)
                .Where(art => art.Invalidated == false)
                .ToList();

            foreach (var token in tokens)
                token.Invalidated = true;

            _db.SaveChanges();
        }

        public async Task<AccountToken> GetTokenAsync(string token)
        {
            return await _db.Set<AccountToken>()
                .Where(art => art.Value.Equals(token))
                .Where(art => art.Invalidated == false)
                .FirstOrDefaultAsync();
        }

        public async Task<int> UpdateAsync(AccountToken accountRefreshToken)
        {
            _db.Set<AccountToken>().Update(accountRefreshToken);
            return await _db.SaveChangesAsync();
        }

        public async Task<Account> GetByIdAsync(int accountId)
        {
            return await _db.Set<Account>()
                                .FirstOrDefaultAsync(a => a.Id == accountId);
        }
    }
}
