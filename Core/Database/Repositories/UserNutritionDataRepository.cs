﻿using Core.Database.Context;
using Core.Database.Entities;
using Core.Dtos;
using Infrastructure.Base;
using System.Linq;

namespace Core.Repositories
{
    public class UserDataRepository : BaseRepository<UserData>
    {
        private DatabaseContext _context { get; set;}

        public UserDataRepository(DatabaseContext context) : base(context)
        {
            _context = context;
        }

        public int GetCaloriesNeeded(int userId)
        {
            return _context.UserMacrosData
                .Where(e => e.AccountId == userId)
                .OrderByDescending(e => e.CreatedAt)
                .Select(e => e.DailyCaloriesIntake)
                .FirstOrDefault();
        }
    }
}
