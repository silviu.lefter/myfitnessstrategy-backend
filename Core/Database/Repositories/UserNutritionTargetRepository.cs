﻿using Core.Database.Context;
using Core.Database.Entities;
using Infrastructure.Base;
using Infrastructure.Exceptions;
using System.Linq;

namespace Core.Database.Repositories
{
    public class UserNutritionTargetRepository : BaseRepository<UserMacrosData>
    {
        private DatabaseContext _context { get; set; }
        public UserNutritionTargetRepository(DatabaseContext context) : base(context)
        {
            _context = context;
        }

        public UserMacrosData GetUserNutrientsTarget(int userId)
        {
            var entity = _context.UserMacrosData
                .Where(e => e.AccountId == userId)
                .OrderByDescending(e => e.CreatedAt)
                .FirstOrDefault();

            if (entity == null)
                throw new ResourceMissingException("Target Nutrition with userId " + userId + " doesn't exist.");

            return entity;
        }
    }
}
